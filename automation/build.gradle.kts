plugins {
    `maven-publish`
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
}

android {
    compileSdk = 31

    defaultConfig {
        minSdk = 26
        targetSdk = 31

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles.add(projectDir.resolve("consumer-rules.pro"))
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles.addAll(
                mutableListOf(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    projectDir.resolve("proguard-rules.pro")
                )
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlinOptions {
        jvmTarget = "11"
    }
}

dependencies {
    compileOnly("androidx.appcompat:appcompat:1.4.1")
    compileOnly("junit:junit:4.13.2")
    compileOnly("androidx.test.ext:junit:1.1.3")
    compileOnly("androidx.test:runner:1.4.0")
    compileOnly("androidx.test.espresso:espresso-core:3.4.0")
}

tasks {
    val sourcesJar by creating(Jar::class) {
        from(android.sourceSets.getByName("main").java.srcDirs)
        archiveClassifier.set("sources")
    }
    artifacts {
        archives(sourcesJar)
    }
}

afterEvaluate {
    publishing {
        publications.create<MavenPublication>("release") {
            from(components["release"])
            groupId = "app.lemley.test-driven-droids"
            artifactId = "automation"
            artifact(tasks.getByName("sourcesJar"))
        }
        repositories {
            repositories {
                maven("https://gitlab.com/api/v4/projects/29817287/packages/maven") {
                    name = "GitLab"
                    val tokenName =
                        properties.getOrDefault("gitLabTokenName", "Job-Token").toString()
                    val tokenValue =
                        properties.getOrDefault("gitLabPrivateToken", System.getenv("CI_JOB_TOKEN"))
                            .toString()
                    credentials(HttpHeaderCredentials::class) {
                        name = tokenName
                        value = tokenValue
                    }
                    authentication {
                        create<HttpHeaderAuthentication>("header")
                    }
                }
            }
        }
    }
}

fun MavenPublication.configurePom() = pom {
    name.set("Test Driven Droids Automation")
    description.set(
        """
            Test Driven Droids Automation
        """.trimIndent()
    )

    developers {
        developer {
            id.set("@mlemley")
            name.set("Michael Lemley")
            email.set("mlemleyap@gmail.com")
            roles.set(listOf("Owner, Maintainer"))
            timezone.set("-6")
        }
    }

    licenses {
        license {
            name.set("The MIT License (MIT)")
            url.set("http://opensource.org/licenses/MIT")
            distribution.set("repo")
        }
    }

    val glPath = "lemley/test-driven-droids/android-automation-framework.git"
    scm {
        connection.set("scm:git@gitlab.com:$glPath")
        developerConnection.set("scm:git@gitlab.com:$glPath")
        url.set("https://gitlab.com/lemley/test-driven-droids/android-automation-framework")
    }
}

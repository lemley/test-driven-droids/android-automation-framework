package testing.droids.automation.rule

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import org.junit.rules.TestRule
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import org.junit.runners.model.Statement
import testing.droids.automation.rule.statement.UiAutomationStatement

class UiAutomationRule : TestRule {

    private val application: Application = ApplicationProvider.getApplicationContext()

    override fun apply(base: Statement, description: Description): Statement =
        UiAutomationStatement(base, description)

    fun stringFor(stringResourceId: Int, vararg formatArgs: Any): String {
        return application.getString(stringResourceId, formatArgs)
    }
}
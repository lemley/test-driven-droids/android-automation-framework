package testing.droids.automation.rule.statement

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.IdlingRegistry
import org.junit.runner.Description
import org.junit.runners.model.Statement
import testing.droids.automation.rule.annotation.GivenActivity
import testing.droids.automation.subject.UiAutomationLifecycle
import kotlin.reflect.KClass

class UiAutomationStatement(
    private val base: Statement,
    private val description: Description,
    private val idlingRegistry: IdlingRegistry = IdlingRegistry.getInstance(),
    private val uiAutomationLifecycle: UiAutomationLifecycle = UiAutomationLifecycle.getInstance()
) : Statement() {

    private var activityScenario: ActivityScenario<*>? = null

    override fun evaluate() {
        before()
        base.evaluate()
        after()
    }

    private fun before() {
        registerIdlingResources()
        launchScenario()
    }

    private fun after() {
        teardownScenario()
        unregisterIdlingResources()
    }

    private fun launchScenario() {
        description.annotations
            .filterIsInstance<GivenActivity>()
            .firstOrNull()?.entryPoint?.let {
                activityScenario = ActivityScenario.launch((it as KClass<AppCompatActivity>).java)
            } ?: Log.e(
            this.javaClass.simpleName,
            "Rule missing givenActivity annotation with entrypoint"
        )
    }

    private fun teardownScenario() {
        activityScenario?.close()
        activityScenario = null
    }

    private fun registerIdlingResources() {
        idlingRegistry.register(*uiAutomationLifecycle.idlingResources().toTypedArray())
    }

    private fun unregisterIdlingResources() {
        idlingRegistry.unregister(*uiAutomationLifecycle.idlingResources().toTypedArray())
    }

}

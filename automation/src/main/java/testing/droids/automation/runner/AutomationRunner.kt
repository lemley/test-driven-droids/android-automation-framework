package testing.droids.automation.runner

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner

class AutomationRunner : AndroidJUnitRunner() {

    /**
     * Looks for Test{Application} first, falling back on App's default
     *
     * @return [Application] instance to test
     */
    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application {
        if (cl != null && className != null) {
            val applicationPath = className.split(".").toMutableList()
            applicationPath[applicationPath.size - 1] = "Automated${applicationPath.last()}"
            val testAppName: String = applicationPath.joinToString(".")

            val appName = try {
                cl.loadClass(testAppName).newInstance()
                testAppName
            } catch (e: Exception) {
                e.printStackTrace()
                className
            }
            println("loading appName: $appName")
            return super.newApplication(cl, appName, context)
        } else {
            return super.newApplication(cl, className, context)
        }
    }
}

package testing.droids.automation.subject

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.IdlingResource

interface UiAutomationLifecycle {

    fun idlingResources(): List<IdlingResource> = emptyList()

    fun testingContext(): TestingContext

    companion object {

        fun getInstance(): UiAutomationLifecycle =
            with(ApplicationProvider.getApplicationContext<Application>()) {
                if (this is UiAutomationLifecycle) this
                else NullUiAutomationLifecycle()
            }

        fun getInstanceOrThrow(): UiAutomationLifecycle =
            with(ApplicationProvider.getApplicationContext<Application>()) {
                if (this is UiAutomationLifecycle) this
                else throw IllegalStateException("Test Application does not Implement `UiAutomationLifecycle`")
            }
    }

    private class NullUiAutomationLifecycle : UiAutomationLifecycle {
        override fun idlingResources(): List<IdlingResource> = emptyList()

        override fun testingContext(): TestingContext = object : TestingContext {}

    }
}
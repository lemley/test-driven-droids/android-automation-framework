package testing.droids.automation.subject

interface TestingContext

fun <T : TestingContext> withTestingContext(block: T.() -> Unit) =
    contextForTestCase<T>().block()

private fun <T : TestingContext> contextForTestCase(): T =
    UiAutomationLifecycle.getInstanceOrThrow()
        .testingContext() as T

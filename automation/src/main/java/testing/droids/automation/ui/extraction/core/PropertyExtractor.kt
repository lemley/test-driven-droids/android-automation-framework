package testing.droids.automation.ui.extraction.core

fun interface PropertyExtractor<Value, Source> {

    fun extractPropertyFrom(
        view: Source,
        which: ExtractionProperty,
        extractionValue: ExtractionValue<Value>
    )
}
package testing.droids.automation.ui.element

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.Matcher
import testing.droids.automation.ui.extraction.core.ExtractionProperty
import testing.droids.automation.ui.extraction.core.ExtractionValue
import testing.droids.automation.ui.extraction.core.PropertyExtractor
import testing.droids.automation.ui.extraction.drawableExtractor
import testing.droids.automation.ui.matching.DrawableValueMatcher

class ImageViewElement<Source : ImageView> internal constructor(
    viewMatcher: Matcher<View>
) : ViewElement<Source>(viewMatcher) {

    override val constraints: Matcher<View> = isAssignableFrom(ImageView::class.java)

    private val drawablePropertyExtractor =
        PropertyExtractor { view: ImageView, which: ExtractionProperty, extractionValue: ExtractionValue<Drawable> ->
            if (which == Properties.Src) {
                extractionValue.assign(view.drawable)
            }

        }

    val src: DrawableValueMatcher = drawableExtractor(
        this,
        Properties.Src,
        "Imageview.src",
        drawablePropertyExtractor
    )

    sealed class Properties : ExtractionProperty {
        object Src : Properties()
    }

    companion object {
        fun <Source : ImageView> imageViewElement(id: Int): ImageViewElement<Source> =
            ImageViewElement(withId(id))

        fun <Source : ImageView> imageViewElement(viewMatcher: Matcher<View>): ImageViewElement<Source> =
            ImageViewElement(viewMatcher)
    }
}
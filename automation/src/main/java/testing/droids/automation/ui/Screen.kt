package testing.droids.automation.ui

import androidx.test.espresso.matcher.ViewMatchers.assertThat
import org.hamcrest.CoreMatchers.equalTo


abstract class Screen {
    abstract fun waitForPageToLoad()
    abstract fun isOnPage(): Boolean

    fun assertIsOnScreen() {
        assertThat(isOnPage(), equalTo(true))
    }
}

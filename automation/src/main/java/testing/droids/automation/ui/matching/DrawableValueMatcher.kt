package testing.droids.automation.ui.matching

import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.DrawableRes
import org.hamcrest.Matcher
import org.hamcrest.Matchers

interface DrawableValueMatcher : Matchable {

    fun extractValue(): Drawable?

    fun assertEqualTo(@DrawableRes id: Int) {
        checkMatches(drawableMatches(id))
    }

    fun drawableMatches(@DrawableRes id: Int): Matcher<View>

}
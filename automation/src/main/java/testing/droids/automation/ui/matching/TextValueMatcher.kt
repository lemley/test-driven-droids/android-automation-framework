package testing.droids.automation.ui.matching

import android.app.Application
import android.view.View
import androidx.annotation.StringRes
import androidx.test.core.app.ApplicationProvider
import org.hamcrest.Matcher
import org.hamcrest.Matchers

interface TextValueMatcher : Matchable {

    fun extractValue(): String?

    fun textMatches(matcher: Matcher<String>): Matcher<View>

    fun assertEqualTo(@StringRes id: Int) {
        assertEqualTo(ApplicationProvider.getApplicationContext<Application>().getString(id))
    }

    fun assertEqualTo(value: String) {
        checkMatches(textMatches(Matchers.equalTo(value)))
    }


}
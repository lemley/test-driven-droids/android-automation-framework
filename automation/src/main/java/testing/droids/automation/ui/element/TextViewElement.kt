package testing.droids.automation.ui.element

import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.Matcher
import testing.droids.automation.ui.element.TextViewElement.TextViewProperties.Booleans.*
import testing.droids.automation.ui.extraction.*
import testing.droids.automation.ui.extraction.core.ExtractionProperty
import testing.droids.automation.ui.extraction.core.ExtractionValue
import testing.droids.automation.ui.extraction.core.PropertyExtractor
import testing.droids.automation.ui.matching.BooleanValueMatcher
import testing.droids.automation.ui.matching.TextValueMatcher
import java.lang.IllegalStateException

open class TextViewElement<Source : TextView> internal constructor(
    viewMatcher: Matcher<View>
) : ViewElement<Source>(viewMatcher) {

    override val viewDescriptor: String = "TextView"

    override val constraints: Matcher<View> = ViewMatchers.isAssignableFrom(TextView::class.java)

    protected val stringPropertyExtractor =
        PropertyExtractor { view: TextView, which: ExtractionProperty, extractionValue: ExtractionValue<String> ->
            when (which) {
                TextViewProperties.Strings.Text -> extractionValue.assign(view.text.toString())
                TextViewProperties.Strings.Hint -> extractionValue.assign(view.hint.toString())
                else -> throw IllegalStateException("Not Implemented")
            }
        }

    private val textViewBooleanPropertyExtractor =
        PropertyExtractor { view: EditText, which: ExtractionProperty, extractionValue: ExtractionValue<Boolean> ->
            when (which) {
                IsRegularText ->
                    extractionValue.assign(view.checkInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_NORMAL))

                IsAllCaps ->
                    extractionValue.assign(view.checkInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS))

                DoesCapitalizeWords ->
                    extractionValue.assign(view.checkInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS))

                DoesCapitalizeSentences ->
                    extractionValue.assign(view.checkInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES))

                IsEmailAddress ->
                    extractionValue.assign(view.checkInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS))

                IsPassword ->
                    extractionValue.assign(view.checkInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD))

                IsPasswordVisible ->
                    extractionValue.assign(view.checkInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD))

                IsOnlyNumbers ->
                    extractionValue.assign(view.checkInputType(InputType.TYPE_NUMBER_VARIATION_NORMAL or InputType.TYPE_CLASS_NUMBER))

                IsPhoneNumber -> extractionValue.assign(view.checkInputType(InputType.TYPE_CLASS_PHONE))
                else -> throw IllegalStateException("Not Implemented")
            }
        }

    val text: TextValueMatcher = stringExtractor(
        this,
        TextViewProperties.Strings.Text,
        "$viewDescriptor.text",
        stringPropertyExtractor
    )

    val hint: TextValueMatcher = stringExtractor(
        this,
        TextViewProperties.Strings.Hint,
        "$viewDescriptor.hint",
        stringPropertyExtractor
    )

    val isRegularText: BooleanValueMatcher = booleanExtractor(
        this,
        IsRegularText,
        "$viewDescriptor.isRegularText",
        textViewBooleanPropertyExtractor
    )

    val isAllCaps: BooleanValueMatcher = booleanExtractor(
        this,
        IsAllCaps,
        "$viewDescriptor.isAllCaps",
        textViewBooleanPropertyExtractor
    )

    val doesCapitalizeWords: BooleanValueMatcher = booleanExtractor(
        this,
        DoesCapitalizeWords,
        "$viewDescriptor.doesCapitalizeWords",
        textViewBooleanPropertyExtractor
    )

    val doesCapitalizeSentences: BooleanValueMatcher = booleanExtractor(
        this,
        DoesCapitalizeSentences,
        "$viewDescriptor.doesCapitalizeSentences",
        textViewBooleanPropertyExtractor
    )

    val isEmailAddress: BooleanValueMatcher = booleanExtractor(
        this,
        IsEmailAddress,
        "$viewDescriptor.isEmailAddress",
        textViewBooleanPropertyExtractor
    )

    val isPassword: BooleanValueMatcher = booleanExtractor(
        this,
        IsPassword,
        "$viewDescriptor.isPassword",
        textViewBooleanPropertyExtractor
    )

    val isPasswordVisible: BooleanValueMatcher = booleanExtractor(
        this,
        IsPasswordVisible,
        "$viewDescriptor.isPasswordVisible",
        textViewBooleanPropertyExtractor
    )

    val isOnlyNumbers: BooleanValueMatcher = booleanExtractor(
        this,
        IsOnlyNumbers,
        "$viewDescriptor.isOnlyNumbers",
        textViewBooleanPropertyExtractor
    )

    val isPhoneNumber: BooleanValueMatcher = booleanExtractor(
        this,
        IsPhoneNumber,
        "$viewDescriptor.isPhoneNumber",
        textViewBooleanPropertyExtractor
    )

    sealed class TextViewProperties : ExtractionProperty {
        sealed class Strings : TextViewProperties() {
            object Text : Strings()
            object Hint : Strings()
        }

        sealed class Booleans : TextViewProperties() {
            object IsRegularText : Booleans()
            object IsAllCaps : Booleans()
            object DoesCapitalizeWords : Booleans()
            object DoesCapitalizeSentences : Booleans()
            object IsEmailAddress : Booleans()
            object IsPassword : Booleans()
            object IsPasswordVisible : Booleans()
            object IsOnlyNumbers : Booleans()
            object IsPhoneNumber : Booleans()
        }

        sealed class Integers : TextViewProperties() {
            object MaxLines : Integers()
            object MinLines : Integers()

        }
    }

    private fun EditText.checkInputType(inputType: Int) = (this.inputType and inputType) == inputType

    companion object {
        fun <Source : TextView> textViewElement(id: Int): TextViewElement<Source> =
            TextViewElement(withId(id))

        fun <Source : TextView> textViewElement(viewMatcher: Matcher<View>): TextViewElement<Source> =
            TextViewElement(viewMatcher)

    }
}

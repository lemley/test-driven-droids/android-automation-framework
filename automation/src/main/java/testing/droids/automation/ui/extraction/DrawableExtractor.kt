package testing.droids.automation.ui.extraction

import android.app.Application
import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.BoundedMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import testing.droids.automation.ui.element.ViewElement
import testing.droids.automation.ui.extraction.core.ExtractionProperty
import testing.droids.automation.ui.extraction.core.ExtractionValue
import testing.droids.automation.ui.extraction.core.PropertyExtractor
import testing.droids.automation.ui.extraction.core.extractableViewAction
import testing.droids.automation.ui.matching.DrawableValueMatcher

class DrawableExtractor<Source : View>(
    val sourceClass: Class<Source>,
    element: ViewElement<*>,
    private val matcherDescription: String,
    private val extractionProperty: ExtractionProperty,
    private val extractor: PropertyExtractor<Drawable, Source>
) : DrawableValueMatcher {

    private val viewInteraction = element.viewInteraction
    private val viewConstraints = element.constraints

    override fun extractValue(): Drawable? {
        val extractionValue = ExtractionValue<Drawable>()
        viewInteraction.perform(
            extractableViewAction(viewConstraints, matcherDescription) { uiController, view ->
                extractor.extractPropertyFrom(
                    view as Source, extractionProperty, extractionValue
                )
            }
        )
        return extractionValue.get()
    }

    override fun drawableMatches(@DrawableRes id: Int): Matcher<View> =
        object : BoundedMatcher<View, Source>(sourceClass) {
            override fun describeTo(description: Description) {
                description.appendText("$matcherDescription ")
                val expectedResourceName: String = ApplicationProvider
                    .getApplicationContext<Application>()
                    .resources.getResourceEntryName(id)
                description.appendText("to have drawable resource id of: ")
                description.appendValue("R.drawable.$expectedResourceName")
            }

            override fun describeMismatch(item: Any, description: Description) {
                val view = item as Source
                val viewId: String = ApplicationProvider
                    .getApplicationContext<Application>()
                    .resources.getResourceEntryName(view.id)
                description.appendText("A different drawable's contents for ${view.javaClass.name} idetntified by: R.id.$viewId")
            }

            override fun matchesSafely(item: Source): Boolean {
                val expectedDrawable: Drawable? = with(item.context) {
                    ResourcesCompat.getDrawable(resources, id, theme)
                }
                val extractionValue = ExtractionValue<Drawable>()
                extractor.extractPropertyFrom(item, extractionProperty, extractionValue)
                val actualDrawable = extractionValue.get()

                return expectedDrawable?.constantState?.let { expected ->
                    actualDrawable?.constantState?.let { actual ->
                        return expected == actual
                    } ?: false
                } ?: actualDrawable == null || actualDrawable?.constantState == null
            }

        }

    override fun checkMatches(matcher: Matcher<View>) {
        viewInteraction.check(ViewAssertions.matches(matcher))
    }

}

inline fun <reified Source : View> drawableExtractor(
    element: ViewElement<*>,
    extractionProperty: ExtractionProperty,
    matcherDescription: String,
    extractor: PropertyExtractor<Drawable, Source>
): DrawableExtractor<Source> = DrawableExtractor(
    Source::class.java,
    element,
    matcherDescription,
    extractionProperty,
    extractor
)
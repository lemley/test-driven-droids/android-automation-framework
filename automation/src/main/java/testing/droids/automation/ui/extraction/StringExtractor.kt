package testing.droids.automation.ui.extraction

import android.view.View
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.BoundedMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import testing.droids.automation.ui.element.ViewElement
import testing.droids.automation.ui.extraction.core.ExtractionProperty
import testing.droids.automation.ui.extraction.core.ExtractionValue
import testing.droids.automation.ui.extraction.core.PropertyExtractor
import testing.droids.automation.ui.extraction.core.extractableViewAction
import testing.droids.automation.ui.matching.TextValueMatcher

/**
 * based largely on review of
 * https://stackoverflow.com/questions/45597008/espresso-get-text-of-element
 *
 * @param actionDescription of value being extracted i.g.("TextView.text")
 */
class StringExtractor<Source : View>(
    val sourceClass: Class<Source>,
    element: ViewElement<*>,
    private val extractionProperty: ExtractionProperty,
    private val matcherDescription: String,
    private val extractor: PropertyExtractor<String, Source>
) : TextValueMatcher {

    private val viewInteraction = element.viewInteraction
    private val viewConstraints = element.constraints

    override fun extractValue(): String? {
        val extractionValue = ExtractionValue<String>()
        viewInteraction.perform(
            extractableViewAction(viewConstraints, matcherDescription) { uiController, view ->
                extractor.extractPropertyFrom(
                    view as Source, extractionProperty, extractionValue
                )
            }
        )
        return extractionValue.get()
    }

    override fun textMatches(matcher: Matcher<String>): Matcher<View> =
        object : BoundedMatcher<View, Source>(sourceClass) {
            override fun describeTo(description: Description) {
                description.appendText("$matcherDescription ")
                matcher.describeTo(description)
            }

            override fun describeMismatch(item: Any, description: Description) {
                val source = item as Source
                description.appendText("$matcherDescription ")
                val extractionValue = ExtractionValue<String>()
                extractor.extractPropertyFrom(source, extractionProperty, extractionValue)
                description.appendValue(extractionValue.get())
            }

            override fun matchesSafely(item: Source): Boolean {
                val extractionValue = ExtractionValue<String>()
                extractor.extractPropertyFrom(item, extractionProperty, extractionValue)
                return matcher.matches(extractionValue.get())
            }

        }

    override fun checkMatches(matcher: Matcher<View>) {
        viewInteraction.check(ViewAssertions.matches(matcher))
    }

}

inline fun <reified Source : View> stringExtractor(
    element: ViewElement<*>,
    extractionProperty: ExtractionProperty,
    matcherDescription: String,
    extractor: PropertyExtractor<String, Source>
): StringExtractor<Source> = StringExtractor(
    Source::class.java,
    element,
    extractionProperty,
    matcherDescription,
    extractor
)
package testing.droids.automation.ui.matching

import android.view.View
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import org.hamcrest.Matcher

interface Matchable {

    fun checkMatches(matcher: Matcher<View>)

}
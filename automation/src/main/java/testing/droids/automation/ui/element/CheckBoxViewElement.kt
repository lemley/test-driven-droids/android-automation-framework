package testing.droids.automation.ui.element

import android.view.View
import android.widget.CheckBox
import androidx.annotation.IdRes
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.Matcher
import testing.droids.automation.ui.extraction.booleanExtractor
import testing.droids.automation.ui.extraction.core.ExtractionProperty
import testing.droids.automation.ui.extraction.core.ExtractionValue
import testing.droids.automation.ui.extraction.core.PropertyExtractor
import testing.droids.automation.ui.extraction.stringExtractor
import testing.droids.automation.ui.matching.BooleanValueMatcher
import testing.droids.automation.ui.matching.TextValueMatcher

class CheckBoxViewElement<Source : CheckBox> internal constructor(
    viewMatcher: Matcher<View>
) : TextViewElement<Source>(viewMatcher) {

    override val viewDescriptor: String = "CheckBox"
    override val constraints: Matcher<View> = isAssignableFrom(CheckBox::class.java)

    private val checkBoxBooleanPropertyExtractor =
        PropertyExtractor { view: CheckBox, which: ExtractionProperty, extractionValue: ExtractionValue<Boolean> ->
            when (which) {
                CheckboxProperties.Booleans.IsChecked -> extractionValue.assign(view.isChecked)
                else -> throw IllegalStateException("Not Implemented")
            }
        }

    val isChecked: BooleanValueMatcher = booleanExtractor(
        this,
        CheckboxProperties.Booleans.IsChecked,
        "$viewDescriptor.isChecked",
        checkBoxBooleanPropertyExtractor
    )

    val label: TextValueMatcher = stringExtractor(
        this,
        TextViewProperties.Strings.Text,
        "$viewDescriptor.text",
        stringPropertyExtractor
    )

    sealed class CheckboxProperties : ExtractionProperty {
        sealed class Booleans : CheckboxProperties() {
            object IsChecked : Booleans()
        }
    }

    companion object {
        fun <Source : CheckBox> checkBoxViewElement(@IdRes id: Int): CheckBoxViewElement<Source> =
            CheckBoxViewElement(withId(id))

        fun <Source : CheckBox> checkBoxViewElement(viewMatcher: Matcher<View>): CheckBoxViewElement<Source> =
            CheckBoxViewElement(viewMatcher)
    }

}

package testing.droids.automation.ui.element

import android.view.View
import android.widget.Button
import androidx.annotation.IdRes
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.Matcher

class ButtonViewElement<Source : Button> internal constructor(
    viewMatcher: Matcher<View>
) : TextViewElement<Source>(viewMatcher) {

    override val viewDescriptor: String = "Button"
    override val constraints: Matcher<View> = isAssignableFrom(Button::class.java)

    companion object {
        fun <Source : Button> buttonViewElement(@IdRes id: Int): ButtonViewElement<Source> =
            ButtonViewElement(withId(id))

        fun <Source : Button> buttonViewElement(viewMatcher: Matcher<View>): ButtonViewElement<Source> =
            ButtonViewElement(viewMatcher)
    }

}

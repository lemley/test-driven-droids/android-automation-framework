package testing.droids.automation.ui.action

import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.ViewMatchers
import org.hamcrest.Matcher

class InformationAction(private val matcher: Matcher<View>) : ViewAction {

    var value: Boolean = false
        private set

    override fun getConstraints(): Matcher<View> = ViewMatchers.isAssignableFrom(View::class.java)

    override fun getDescription(): String = "Compares given matcher to the view"

    override fun perform(uiController: UiController?, view: View?) {
        value = try {
            matcher.matches(view)
            true
        } catch (e: Exception) {
            false
        }
    }

}

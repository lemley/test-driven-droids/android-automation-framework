package testing.droids.automation.ui.element

import android.view.View
import android.widget.EditText
import androidx.annotation.IdRes
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.Matcher

class EditTextViewElement<Source : EditText> internal constructor(
    viewMatcher: Matcher<View>
) : TextViewElement<Source>(viewMatcher) {

    override val viewDescriptor: String = "EditText"
    override val constraints: Matcher<View> = isAssignableFrom(EditText::class.java)

    fun performReplaceText(text: String) {
        viewInteraction.perform(replaceText(text))
    }

    companion object {
        fun <Source : EditText> editTextViewElement(@IdRes id: Int): EditTextViewElement<Source> =
            EditTextViewElement(withId(id))

        fun <Source : EditText> editTextViewElement(viewMatcher: Matcher<View>): EditTextViewElement<Source> =
            EditTextViewElement(viewMatcher)
    }

}

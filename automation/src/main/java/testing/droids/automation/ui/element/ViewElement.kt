package testing.droids.automation.ui.element

import android.view.View
import androidx.annotation.IdRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.Root
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.Matcher
import testing.droids.automation.ui.extraction.booleanExtractor
import testing.droids.automation.ui.extraction.core.ExtractionProperty
import testing.droids.automation.ui.extraction.core.ExtractionValue
import testing.droids.automation.ui.extraction.core.PropertyExtractor
import testing.droids.automation.ui.matching.BooleanValueMatcher
import org.hamcrest.Matchers
import testing.droids.automation.ui.action.InformationAction
import testing.droids.automation.ui.matching.Matchable
import java.util.concurrent.TimeoutException

/**
 * @param viewMatcher [Matcher<View>] i.g. withId(R.id.foo)
 * @param constraints [Matcher<View>] i.g. isAssignableFrom(View::Class.java)
 */
open class ViewElement<Source : View>(
    private val viewMatcher: Matcher<View>,
    private val ancestor: ViewElement<View>? = null
) : Matchable {

    protected open val viewDescriptor: String = "View"
    var root: Matcher<Root>? = null
    val fullMatcher: Matcher<View> = buildMatcher()
    var closeSoftKeyboard: Boolean = true

    open val constraints: Matcher<View> = isAssignableFrom(View::class.java)

    open val viewInteraction: ViewInteraction = onView(viewMatcher)

    protected val booleanPropertyExtractor =
        PropertyExtractor { view: View, which: ExtractionProperty, extractionValue: ExtractionValue<Boolean> ->
            when (which) {
                Properties.Booleans.IsVisible -> extractionValue.assign(view.visibility == View.VISIBLE)
                Properties.Booleans.IsGone -> extractionValue.assign(view.visibility == View.GONE)
                Properties.Booleans.IsInvisible -> extractionValue.assign(view.visibility == View.INVISIBLE)
                else -> throw IllegalStateException("Not Implemented")
            }
        }

    fun assertIsEnabled() {
        checkMatches(ViewMatchers.isEnabled())
    }

    fun assertIsDisplayed() {
        checkMatches(ViewMatchers.isDisplayed())
    }

    fun assertIsCompletelyDisplayed() {
        checkMatches(ViewMatchers.isCompletelyDisplayed())
    }

    fun isDisplayed(): Boolean {
        return try {
            matches(ViewMatchers.isDisplayed())
        } catch (e: TimeoutException) {
            false
        } catch (e: NoMatchingViewException) {
            false
        }
    }

    fun performClick() {
        perform(ViewActions.click())
    }

    fun closeSoftKeyboard() {
        performUnsafe(ViewActions.closeSoftKeyboard())
    }

    protected fun perform(vararg actions: ViewAction) {
        getInteractionSafe().perform(*actions)
    }

    protected fun performUnsafe(vararg actions: ViewAction) {
        getInteractionUnsafe().perform(*actions)
    }

    protected fun getInteractionSafe(): ViewInteraction {
        ensureVisible()
        return getInteractionUnsafe()
    }

    protected fun getInteractionUnsafe(): ViewInteraction {
        if (root == null) {
            root = ancestor?.root
        }
        return with(onView(fullMatcher)) { root?.let { inRoot(it) } ?: this }
    }

    protected fun ensureVisible() {
        waitUntilDisplayed()
        closeSoftKeyboard()
    }

    protected fun matches(matcher: Matcher<View>, isSafe: Boolean = true): Boolean {
        val action = InformationAction(matcher)
        if (isSafe) perform(action)
        else performUnsafe(action)
        return action.value
    }

    /**
     * TODO Figure out an appropriate way to do this
     * Assertions fail when view isn't immediately available
     * Idling resources will not be enough
     *       i.g. (splash screens with delays)
     * [Screen]s will for sure have to wait with an backoff / cliff
     * or come up with a generic idling resource to add some buffer
     * for apps to implement during async work
     */
    fun waitUntilDisplayed() {

    }


    private fun buildMatcher(): Matcher<View> = ancestor?.let {
        Matchers.allOf(viewMatcher, ViewMatchers.isDescendantOfA(ancestor.buildMatcher()))
    } ?: viewMatcher

    override fun checkMatches(matcher: Matcher<View>) {
        viewInteraction.check(ViewAssertions.matches(matcher))
    }

    val isVisible: BooleanValueMatcher = booleanExtractor(
        this,
        Properties.Booleans.IsVisible,
        "$viewDescriptor.isVisible",
        booleanPropertyExtractor
    )

    val isGone: BooleanValueMatcher = booleanExtractor(
        this,
        Properties.Booleans.IsGone,
        "$viewDescriptor.isGone",
        booleanPropertyExtractor
    )

    val isInvisible: BooleanValueMatcher = booleanExtractor(
        this,
        Properties.Booleans.IsInvisible,
        "$viewDescriptor.isInvisible",
        booleanPropertyExtractor
    )

    sealed class Properties : ExtractionProperty {
        sealed class Booleans : Properties() {
            object IsVisible : Booleans()
            object IsGone : Booleans()
            object IsInvisible : Booleans()
        }
    }

    companion object {

        fun <Source : View> viewElement(
            @IdRes id: Int,
            ancestor: ViewElement<View>? = null
        ): ViewElement<Source> = ViewElement(withId(id), ancestor)
    }

}

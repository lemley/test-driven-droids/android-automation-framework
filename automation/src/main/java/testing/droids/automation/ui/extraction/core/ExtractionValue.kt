package testing.droids.automation.ui.extraction.core

class ExtractionValue<Value> {
    private var value: Value? = null

    fun assign(value: Value?) {
        this.value = value
    }

    fun get(): Value? = value
}
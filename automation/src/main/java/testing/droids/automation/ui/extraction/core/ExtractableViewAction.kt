package testing.droids.automation.ui.extraction.core

import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import org.hamcrest.Matcher

class ExtractableViewAction(
    private val viewConstraints: Matcher<View>,
    private val description: String,
    private val extractionBlock: (uiController: UiController, view: View) -> Unit = { _, _ -> }
) : ViewAction {

    override fun getConstraints(): Matcher<View> = viewConstraints

    override fun getDescription(): String = "$description "

    override fun perform(uiController: UiController, view: View) {
        extractionBlock(uiController, view)
    }

}

fun extractableViewAction(
    viewConstraints: Matcher<View>,
    description: String,
    extractionBlock: (uiController: UiController, view: View) -> Unit
): ExtractableViewAction = ExtractableViewAction(viewConstraints, description, extractionBlock)
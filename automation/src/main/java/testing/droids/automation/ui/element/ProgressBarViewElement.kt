package testing.droids.automation.ui.element

import android.view.View
import android.widget.ProgressBar
import androidx.annotation.IdRes
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.Matcher
import testing.droids.automation.ui.extraction.booleanExtractor
import testing.droids.automation.ui.extraction.core.ExtractionProperty
import testing.droids.automation.ui.extraction.core.ExtractionValue
import testing.droids.automation.ui.extraction.core.PropertyExtractor
import testing.droids.automation.ui.matching.BooleanValueMatcher

class ProgressBarViewElement<Source : ProgressBar> internal constructor(
    viewMatcher: Matcher<View>
) : ViewElement<Source>(viewMatcher) {

    override val viewDescriptor: String = "ProgressBar"
    override val constraints: Matcher<View> = isAssignableFrom(ProgressBar::class.java)

    private val progressBarBooleanPropertyExtractor =
        PropertyExtractor { view: ProgressBar, which: ExtractionProperty, extractionValue: ExtractionValue<Boolean> ->
            when (which) {
                ProgressBarProperties.Booleans.IsIndeterminate -> extractionValue.assign(view.isIndeterminate)
                ProgressBarProperties.Booleans.IsAnimating -> extractionValue.assign(view.isAnimating)
                else -> throw IllegalStateException("Not Implemented")
            }
        }

    val isIndeterminate: BooleanValueMatcher = booleanExtractor(
        this,
        ProgressBarProperties.Booleans.IsIndeterminate,
        "$viewDescriptor.isIndeterminate",
        progressBarBooleanPropertyExtractor
    )

    val isAnimating: BooleanValueMatcher = booleanExtractor(
        this,
        ProgressBarProperties.Booleans.IsAnimating,
        "$viewDescriptor.isAnimating",
        progressBarBooleanPropertyExtractor
    )

    sealed class ProgressBarProperties : ExtractionProperty {
        sealed class Booleans : ProgressBarProperties() {
            object IsIndeterminate : Booleans()
            object IsAnimating : Booleans()
        }
    }

    companion object {
        fun <Source : ProgressBar> progressBarViewElement(@IdRes id: Int): ProgressBarViewElement<Source> =
            ProgressBarViewElement(withId(id))

        fun <Source : ProgressBar> progressBarViewElement(viewMatcher: Matcher<View>): ProgressBarViewElement<Source> =
            ProgressBarViewElement(viewMatcher)
    }

}

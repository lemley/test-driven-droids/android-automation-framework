package testing.droids.automation.ui.matching

import android.view.View
import org.hamcrest.Matcher
import org.hamcrest.Matchers

interface BooleanValueMatcher : Matchable {

    fun extractValue(): Boolean?

    fun booleanMatches(matcher: Matcher<Boolean>): Matcher<View>

    fun assertEqualTo(value: Boolean) {
        checkMatches(booleanMatches(Matchers.equalTo(value)))
    }

}
package dev.droids.demo.ui.scenario

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import dev.droids.demo.databinding.ItemScenarioSectionContentBinding
import dev.droids.demo.databinding.ItemScenarioSectionTitleBinding
import dev.droids.demo.ui.scenario.ScenarioViewModel.ScenarioUIElement
import dev.droids.demo.ui.scenario.ScenarioViewModel.ScenarioUIElement.SectionItem
import dev.droids.demo.ui.scenario.ScenarioViewModel.ScenarioUIElement.SectionTitle

class ScenarioAdapter : RecyclerView.Adapter<ScenarioAdapter.ViewHolder>() {

    var scenarios: List<ScenarioUIElement> = emptyList()
        set(value) {
            if (value != field) {
                field = value
                notifyDataSetChanged()
            }
        }

    override fun getItemViewType(position: Int): Int = when (scenarios[position]) {
        is SectionItem -> TYPE_CONTENT
        else -> TYPE_TITLE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        when (viewType) {
            TYPE_TITLE -> ViewHolder.SectionTitleHolder(
                ItemScenarioSectionTitleBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            else -> ViewHolder.SectionContentHolder(
                ItemScenarioSectionContentBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
        }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (val uiElement = scenarios[position]) {
            is SectionItem -> (holder as ViewHolder.SectionContentHolder).bindTo(uiElement)
            is SectionTitle -> (holder as ViewHolder.SectionTitleHolder).bindTo(uiElement)
        }
    }

    override fun getItemCount(): Int = scenarios.size

    sealed class ViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {

        class SectionTitleHolder(val binding: ItemScenarioSectionTitleBinding) :
            ViewHolder(binding.root) {

            fun bindTo(item: SectionTitle) {
                binding.title.text = item.title
            }
        }

        class SectionContentHolder(val binding: ItemScenarioSectionContentBinding) :
            ViewHolder(binding.root) {

            fun bindTo(item: SectionItem) {
                binding.apply {
                    content.text = item.label
                    root.setOnClickListener {
                        it.findNavController().navigate(item.navDirections)
                    }
                }
            }
        }

    }

    companion object {
        private const val TYPE_TITLE: Int = 0
        private const val TYPE_CONTENT: Int = 10
    }
}
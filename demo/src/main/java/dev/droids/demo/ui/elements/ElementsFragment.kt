package dev.droids.demo.ui.elements

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import dev.droids.demo.R
import dev.droids.demo.databinding.FragmentStartBinding
import dev.droids.demo.util.ext.exhaustive
import dev.droids.demo.util.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class ElementsFragment : Fragment(R.layout.fragment_elements) {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_elements, container, false)
    }

    override fun onResume() {
        super.onResume()
    }
}
package dev.droids.demo.ui.start

import androidx.lifecycle.*
import dev.droids.demo.ui.ViewState
import dev.droids.demo.util.SingleUseDirections
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flow

sealed class StartViewState : ViewState {
    object Initial : StartViewState()
    data class NavDirections(val singleUseDirections: SingleUseDirections) : StartViewState()
}

class StartViewModel : ViewModel() {

    private val _state: MutableStateFlow<StartViewState> = MutableStateFlow(StartViewState.Initial)

    init {
        viewModelScope.launch(Dispatchers.IO) {
            delay(1500)
            _state.value = StartViewState.NavDirections(
                SingleUseDirections(StartFragmentDirections.actionNavigationStartToNavigationScenario())
            )
        }
    }

    val state: LiveData<StartViewState> = _state.asLiveData()
}

package dev.droids.demo.ui.scenario

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dev.droids.demo.R
import dev.droids.demo.databinding.FragmentScenarioBinding
import dev.droids.demo.util.SingleUseDirections
import dev.droids.demo.util.viewBinding
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ScenarioFragment : Fragment(R.layout.fragment_scenario) {

    private val scenarioAdapter: ScenarioAdapter by inject()
    private val viewModel: ScenarioViewModel by viewModel()
    private val binding: FragmentScenarioBinding by viewBinding(FragmentScenarioBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        scenarioAdapter.scenarios = viewModel.scenarios
        binding.apply {
            binding.scenarios.apply {
                adapter = scenarioAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            }
        }

        binding.navigationElements.setOnClickListener {
            SingleUseDirections(
                ScenarioFragmentDirections.actionNavigationStartToNavigationElements()
            ).executeDirectionsWith(findNavController())
        }
    }
}
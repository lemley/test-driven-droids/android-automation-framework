package dev.droids.demo.ui.scenario

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import dev.droids.demo.R

class ScenarioViewModel(
    context: Context
) : ViewModel() {

    sealed class ScenarioUIElement {
        data class SectionTitle(val title: String) : ScenarioUIElement()
        data class SectionItem(val label: String, val navDirections: NavDirections) :
            ScenarioUIElement()
    }

    val scenarios: List<ScenarioUIElement> = listOf(
        ScenarioUIElement.SectionTitle(context.getString(R.string.scenario_section_title_scrolling)),
        ScenarioUIElement.SectionItem(
            context.getString(R.string.scenario_section_item__in_outer_scroll_view),
            ScenarioFragmentDirections.actionNavigationScenarioToNavigationScenarioOuterScrollView()
        )
    )
}
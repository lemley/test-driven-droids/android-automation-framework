package dev.droids.demo.ui.scenario.scrolling

import androidx.fragment.app.Fragment
import dev.droids.demo.R
import dev.droids.demo.databinding.FragmentScenarioInOuterScrollViewBinding
import dev.droids.demo.util.viewBinding

class InOuterScrollViewFragment : Fragment(R.layout.fragment_scenario_in_outer_scroll_view) {

    private val binding: FragmentScenarioInOuterScrollViewBinding by viewBinding(
        FragmentScenarioInOuterScrollViewBinding::bind
    )
}
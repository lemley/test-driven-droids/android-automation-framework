package dev.droids.demo.app

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.logger.Level

open class DemoApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupDI()
    }

    protected open fun setupDI() {
        startKoin{
            androidLogger(Level.ERROR)
            androidContext(this@DemoApplication)
            modules(appModule)
        }
    }

}

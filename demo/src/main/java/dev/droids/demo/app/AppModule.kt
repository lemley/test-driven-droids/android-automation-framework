package dev.droids.demo.app

import dev.droids.demo.ui.scenario.ScenarioAdapter
import dev.droids.demo.ui.scenario.ScenarioViewModel
import dev.droids.demo.ui.start.StartViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {

    viewModel { StartViewModel() }

    // region Scenario Fragment

    factory { ScenarioAdapter() }
    viewModel { ScenarioViewModel(androidApplication()) }

    // endregion Scenario Fragment


}
package dev.droids.demo.util

import androidx.navigation.NavController
import androidx.navigation.NavDirections

class SingleUseDirections(
    private val directions: NavDirections
) {
    private var hasNotExecuted: Boolean = true

    fun executeDirectionsWith(navController: NavController) {

        if (hasNotExecuted) {
            navController.navigate(directions)
            hasNotExecuted = false
        }

    }
}
package dev.droids.demo.util.ext

val <T> T.exhaustive: T
    get() = this
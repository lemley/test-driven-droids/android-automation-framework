package dev.droids.demo.app

import testing.droids.automation.subject.TestingContext
import testing.droids.automation.subject.UiAutomationLifecycle


class AutomatedDemoApplication : DemoApplication(), UiAutomationLifecycle {

    private val testingContext = DemoTestingContext()

    override fun testingContext(): TestingContext = testingContext
}

package dev.droids.demo.app

import testing.automation.screen.elements.ElementsScreen
import testing.automation.screen.scenario.ScenarioScreen
import testing.automation.screen.start.StartScreen
import testing.droids.automation.subject.TestingContext

class DemoTestingContext : TestingContext {

    fun startScreen(): StartScreen = StartScreen()
    fun scenarioScreen(): ScenarioScreen = ScenarioScreen()
    fun elementsScreen(): ElementsScreen = ElementsScreen()
}

package testing.automation.screen.start

import android.widget.ImageView
import dev.droids.demo.R
import testing.droids.automation.ui.Screen
import testing.droids.automation.ui.element.ImageViewElement
import testing.droids.automation.ui.element.ImageViewElement.Companion.imageViewElement

class StartScreen(
    val logo: ImageViewElement<ImageView> = imageViewElement(R.id.logo)
) : Screen() {

    override fun waitForPageToLoad() {
        logo.waitUntilDisplayed()
    }

    override fun isOnPage(): Boolean = logo.isDisplayed()
}

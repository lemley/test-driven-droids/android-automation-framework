package testing.automation.screen.elements

import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ProgressBar
import dev.droids.demo.R
import testing.droids.automation.ui.Screen
import testing.droids.automation.ui.element.CheckBoxViewElement
import testing.droids.automation.ui.element.CheckBoxViewElement.Companion.checkBoxViewElement
import testing.droids.automation.ui.element.EditTextViewElement
import testing.droids.automation.ui.element.EditTextViewElement.Companion.editTextViewElement
import testing.droids.automation.ui.element.ProgressBarViewElement
import testing.droids.automation.ui.element.ProgressBarViewElement.Companion.progressBarViewElement
import testing.droids.automation.ui.element.ViewElement
import testing.droids.automation.ui.element.ViewElement.Companion.viewElement

class ElementsScreen(
    val checkBox: CheckBoxViewElement<CheckBox> = checkBoxViewElement(R.id.checkbox),

    val progressBar: ProgressBarViewElement<ProgressBar> = progressBarViewElement(R.id.progressBar),

    val viewVisible: ViewElement<View> = viewElement(R.id.viewVisible),
    val viewInvisible: ViewElement<View> = viewElement(R.id.viewInvisible),
    val viewGone: ViewElement<View> = viewElement(R.id.viewGone),

    val editTextRegularText: EditTextViewElement<EditText> = editTextViewElement(R.id.editTextRegularText),
    val editTextAllCaps: EditTextViewElement<EditText> = editTextViewElement(R.id.editTextAllCaps),
    val editTextCapWords: EditTextViewElement<EditText> = editTextViewElement(R.id.editTextCapWords),
    val editTextCapSentences: EditTextViewElement<EditText> = editTextViewElement(R.id.editTextCapSentences),
    val editTextMultiLines: EditTextViewElement<EditText> = editTextViewElement(R.id.editTextMultiLines),
    val editTextEmailAddress: EditTextViewElement<EditText> = editTextViewElement(R.id.editTextEmailAddress),
    val editTextPassword: EditTextViewElement<EditText> = editTextViewElement(R.id.editTextPassword),
    val editTextPasswordVisible: EditTextViewElement<EditText> = editTextViewElement(R.id.editTextPasswordVisible),
    val editTextOnlyNumbers: EditTextViewElement<EditText> = editTextViewElement(R.id.editTextOnlyNumbers),
    val editTextPhoneNumber: EditTextViewElement<EditText> = editTextViewElement(R.id.editTextPhoneNumber),

    val editText: EditTextViewElement<EditText> = editTextViewElement(R.id.editText)

) : Screen
package testing.automation.screen.scenario

import android.widget.Button
import dev.droids.demo.R
import testing.droids.automation.ui.Screen
import testing.droids.automation.ui.element.ButtonViewElement

class ScenarioScreen(
    val elementsButton: ButtonViewElement<Button> = ButtonViewElement.buttonViewElement(R.id.navigation_elements),
    ) : Screen {

    }
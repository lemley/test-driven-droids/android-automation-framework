package testing.integration.ui.start

import androidx.test.ext.junit.runners.AndroidJUnit4
import dev.droids.demo.MainActivity
import dev.droids.demo.R
import dev.droids.demo.app.DemoTestingContext
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import testing.droids.automation.rule.UiAutomationRule
import testing.droids.automation.rule.annotation.GivenActivity
import testing.droids.automation.subject.withTestingContext

@RunWith(AndroidJUnit4::class)
class StartScreenTest {

    @get:Rule
    val automation = UiAutomationRule()

    @Test
    @GivenActivity(
        entryPoint = MainActivity::class
    )
    fun test_it_presents_logo() = withTestingContext<DemoTestingContext> {
        with(startScreen()) {
            logo.assertIsCompletelyDisplayed()
            logo.src.assertEqualTo(R.drawable.android_head)
        }
    }
}

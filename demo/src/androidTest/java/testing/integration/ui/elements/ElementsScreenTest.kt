package testing.integration.ui.elements

import androidx.test.ext.junit.runners.AndroidJUnit4
import dev.droids.demo.MainActivity
import dev.droids.demo.app.DemoTestingContext
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import testing.droids.automation.rule.UiAutomationRule
import testing.droids.automation.rule.annotation.GivenActivity
import testing.droids.automation.subject.withTestingContext

@RunWith(AndroidJUnit4::class)
class ElementsScreenTest {

    @get:Rule
    val automation = UiAutomationRule()

    @Before
    fun setUp() = withTestingContext<DemoTestingContext> {
        Thread.sleep(2000)
        with(scenarioScreen()) {
            elementsButton.performClick()
        }
    }

    @Test
    @GivenActivity<MainActivity>(entryPoint = MainActivity::class)
    fun test_checkbox() = withTestingContext<DemoTestingContext> {
        with(elementsScreen()) {
            checkBox.label.assertEqualTo("Checkbox Label")

            checkBox.isChecked.assertEqualTo(false)
            checkBox.performClick()
            checkBox.isChecked.assertEqualTo(true)
        }
    }

    @Test
    @GivenActivity<MainActivity>(entryPoint = MainActivity::class)
    fun test_progressbar() = withTestingContext<DemoTestingContext> {
        with(elementsScreen()) {
            progressBar.isIndeterminate.assertEqualTo(true)
            progressBar.isAnimating.assertEqualTo(true)
        }
    }

    @Test
    @GivenActivity<MainActivity>(entryPoint = MainActivity::class)
    fun test_view() = withTestingContext<DemoTestingContext> {
        with(elementsScreen()) {
            viewVisible.isVisible.assertEqualTo(true)
            viewVisible.isInvisible.assertEqualTo(false)
            viewVisible.isGone.assertEqualTo(false)

            viewInvisible.isVisible.assertEqualTo(false)
            viewInvisible.isInvisible.assertEqualTo(true)
            viewInvisible.isGone.assertEqualTo(false)

            viewGone.isVisible.assertEqualTo(false)
            viewGone.isInvisible.assertEqualTo(false)
            viewGone.isGone.assertEqualTo(true)
        }
    }

    @Test
    @GivenActivity<MainActivity>(entryPoint = MainActivity::class)
    fun test_editText_inputType() = withTestingContext<DemoTestingContext> {
        with(elementsScreen()) {
            editTextRegularText.isRegularText.assertEqualTo(true)
            editTextRegularText.isAllCaps.assertEqualTo(false)
            editTextRegularText.doesCapitalizeWords.assertEqualTo(false)
            editTextRegularText.doesCapitalizeSentences.assertEqualTo(false)
            editTextRegularText.isEmailAddress.assertEqualTo(false)
            editTextRegularText.isPassword.assertEqualTo(false)
            editTextRegularText.isPasswordVisible.assertEqualTo(false)
            editTextRegularText.isOnlyNumbers.assertEqualTo(false)
            editTextRegularText.isPhoneNumber.assertEqualTo(false)

            editTextAllCaps.isAllCaps.assertEqualTo(true)
            editTextAllCaps.isRegularText.assertEqualTo(true)
            editTextAllCaps.doesCapitalizeWords.assertEqualTo(false)
            editTextAllCaps.doesCapitalizeSentences.assertEqualTo(false)
            editTextAllCaps.isEmailAddress.assertEqualTo(false)
            editTextAllCaps.isPassword.assertEqualTo(false)
            editTextAllCaps.isPasswordVisible.assertEqualTo(false)
            editTextAllCaps.isOnlyNumbers.assertEqualTo(false)
            editTextAllCaps.isPhoneNumber.assertEqualTo(false)

            editTextCapWords.doesCapitalizeWords.assertEqualTo(true)
            editTextCapWords.isRegularText.assertEqualTo(true)
            editTextCapWords.isAllCaps.assertEqualTo(false)
            editTextCapWords.doesCapitalizeSentences.assertEqualTo(false)
            editTextCapWords.isEmailAddress.assertEqualTo(false)
            editTextCapWords.isPassword.assertEqualTo(false)
            editTextCapWords.isPasswordVisible.assertEqualTo(false)
            editTextCapWords.isOnlyNumbers.assertEqualTo(false)
            editTextCapWords.isPhoneNumber.assertEqualTo(false)

            editTextCapSentences.doesCapitalizeSentences.assertEqualTo(true)
            editTextCapSentences.isRegularText.assertEqualTo(true)
            editTextCapSentences.isAllCaps.assertEqualTo(false)
            editTextCapSentences.doesCapitalizeWords.assertEqualTo(false)
            editTextCapSentences.isEmailAddress.assertEqualTo(false)
            editTextCapSentences.isPassword.assertEqualTo(false)
            editTextCapSentences.isPasswordVisible.assertEqualTo(false)
            editTextCapSentences.isOnlyNumbers.assertEqualTo(false)
            editTextCapSentences.isPhoneNumber.assertEqualTo(false)

            editTextMultiLines.isRegularText.assertEqualTo(true)
            editTextMultiLines.isAllCaps.assertEqualTo(false)
            editTextMultiLines.doesCapitalizeWords.assertEqualTo(false)
            editTextMultiLines.doesCapitalizeSentences.assertEqualTo(false)
            editTextMultiLines.isEmailAddress.assertEqualTo(false)
            editTextMultiLines.isPassword.assertEqualTo(false)
            editTextMultiLines.isPasswordVisible.assertEqualTo(false)
            editTextMultiLines.isOnlyNumbers.assertEqualTo(false)
            editTextMultiLines.isPhoneNumber.assertEqualTo(false)

            editTextEmailAddress.isEmailAddress.assertEqualTo(true)
            editTextEmailAddress.isRegularText.assertEqualTo(true)
            editTextEmailAddress.isAllCaps.assertEqualTo(false)
            editTextEmailAddress.doesCapitalizeWords.assertEqualTo(false)
            editTextEmailAddress.doesCapitalizeSentences.assertEqualTo(false)
            editTextEmailAddress.isPassword.assertEqualTo(false)
            editTextEmailAddress.isPasswordVisible.assertEqualTo(false)
            editTextEmailAddress.isOnlyNumbers.assertEqualTo(false)
            editTextEmailAddress.isPhoneNumber.assertEqualTo(false)

            editTextPassword.isPassword.assertEqualTo(true)
            editTextPassword.isRegularText.assertEqualTo(true)
            editTextPassword.isAllCaps.assertEqualTo(false)
            editTextPassword.doesCapitalizeWords.assertEqualTo(false)
            editTextPassword.doesCapitalizeSentences.assertEqualTo(false)
            editTextPassword.isEmailAddress.assertEqualTo(false)
            editTextPassword.isPasswordVisible.assertEqualTo(false)
            editTextPassword.isOnlyNumbers.assertEqualTo(false)
            editTextPassword.isPhoneNumber.assertEqualTo(false)

            editTextPasswordVisible.isPasswordVisible.assertEqualTo(true)
            editTextPasswordVisible.isRegularText.assertEqualTo(true)
            editTextPasswordVisible.isAllCaps.assertEqualTo(false)
            editTextPasswordVisible.doesCapitalizeWords.assertEqualTo(false)
            editTextPasswordVisible.doesCapitalizeSentences.assertEqualTo(false)
            editTextPasswordVisible.isEmailAddress.assertEqualTo(false)
            editTextPasswordVisible.isPassword.assertEqualTo(true)
            editTextPasswordVisible.isOnlyNumbers.assertEqualTo(false)
            editTextPasswordVisible.isPhoneNumber.assertEqualTo(false)

            editTextOnlyNumbers.isOnlyNumbers.assertEqualTo(true)
            editTextOnlyNumbers.isRegularText.assertEqualTo(false)
            editTextOnlyNumbers.isAllCaps.assertEqualTo(false)
            editTextOnlyNumbers.doesCapitalizeWords.assertEqualTo(false)
            editTextOnlyNumbers.doesCapitalizeSentences.assertEqualTo(false)
            editTextOnlyNumbers.isEmailAddress.assertEqualTo(false)
            editTextOnlyNumbers.isPassword.assertEqualTo(false)
            editTextOnlyNumbers.isPasswordVisible.assertEqualTo(false)
            editTextOnlyNumbers.isPhoneNumber.assertEqualTo(false)

            editTextPhoneNumber.isPhoneNumber.assertEqualTo(true)
            editTextPhoneNumber.isRegularText.assertEqualTo(true)
            editTextPhoneNumber.isAllCaps.assertEqualTo(false)
            editTextPhoneNumber.doesCapitalizeWords.assertEqualTo(false)
            editTextPhoneNumber.doesCapitalizeSentences.assertEqualTo(false)
            editTextPhoneNumber.isEmailAddress.assertEqualTo(false)
            editTextPhoneNumber.isPassword.assertEqualTo(false)
            editTextPhoneNumber.isPasswordVisible.assertEqualTo(false)
            editTextPhoneNumber.isOnlyNumbers.assertEqualTo(true)
        }
    }

    @Test
    @GivenActivity<MainActivity>(entryPoint = MainActivity::class)
    fun test_hint_and_text_from_edittext() = withTestingContext<DemoTestingContext> {
        with(elementsScreen()) {
            editText.hint.assertEqualTo("EditText Hint")
            editText.text.assertEqualTo("")

            val updatedText = "New Text"

            editText.performReplaceText(updatedText)
            editText.text.assertEqualTo(updatedText)
        }
    }
}
// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    repositories {
        google()
        mavenCentral()
        maven(url = "https://gitlab.com/api/v4/projects/31335175/packages/maven")
    }


    val navigationVersion by extra("2.4.1")

    dependencies {
        classpath("com.android.tools.build:gradle:7.0.4")
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:$navigationVersion")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.20")
        classpath("app.lemley:android-release-manager:1.6.1.2")

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}
plugins.apply("app.lemley.android-release-manager")

configure<app.lemley.gradle.extension.ReleaseManagerExtension> {
    git {
        provider {
            accessToken.set(System.getenv("GITLAB_ACCESS_TOKEN"))
        }
        rules {
            hotFixRule {
                onlyOn.set(listOf("main"))
                startingWith.set("")
            }
            postReleaseRule {
                startingWith.set("release")
                commitMessage.set("Release")
                appendVersion.set(true)
                cleanupReleaseBranchesOnPostRelease.set(true)
                autoMergeIntoDefaultBranch.set(true)
            }
        }
    }
}
//version = projectDir.resolve("version.txt").readText()
allprojects {
    //version = rootProject.version
}
tasks.create("clean") {
    rootProject.buildDir.delete()
}
